import passive.Management;

public class DummyManagement extends Management {
    public DummyManagement (DummyWarehouse wareHouse){
        super(wareHouse, 0, 0);
    }
}
