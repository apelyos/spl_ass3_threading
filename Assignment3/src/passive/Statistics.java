package passive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import structures.RentalRequest;
import structures.RepairMaterialInformation;
import structures.RepairToolInformation;


/**
 * The Class Statistics.
 */
class Statistics {
	private Double fMoneyGained;
	private ArrayList<RentalRequest> fRentalRequests;
	private Map<String, Integer> fRepairTools;
	private Map<String, Double> fRepairMaterials;
	private final String EOL = "\n";
	
	/**
	 * Instantiates a new statistics object.
	 */
	public Statistics() {
		fMoneyGained = 0.0;
		fRentalRequests = new ArrayList<RentalRequest>();
		fRepairTools = new HashMap<String, Integer>();
		fRepairMaterials = new HashMap<String, Double>();
	}

	/**
	 * Adds the money gained statistics.
	 *
	 * @param amount the amount
	 */
	void addMoneyGained(double amount) {
		synchronized (fMoneyGained) {
			fMoneyGained = fMoneyGained.doubleValue() + amount;
		}
	}
	
	/**
	 * Adds the completed rental request statistics.
	 *
	 * @param rentalRequest the rental request
	 */
	void addCompletedRentalRequest(RentalRequest rentalRequest) {
		synchronized (fRentalRequests) {
			fRentalRequests.add(rentalRequest);
		}
	}
	
	/**
	 * Adds the used tool statistics.
	 *
	 * @param tool the tool
	 */
	void addUsedTool(RepairToolInformation tool) {
		int amount = tool.getQuantityNeededToRepair();
		synchronized (fRepairTools) {
			Integer toolAmount = fRepairTools.get(tool.getName());
			
			if (toolAmount == null) {
				fRepairTools.put(tool.getName(), amount);
			} else {
				fRepairTools.put(tool.getName(), toolAmount.intValue() + amount);
			}
		}
	}

	/**
	 * Adds the consumed material statistics.
	 *
	 * @param material the material
	 */
	void addConsumedMaterial(RepairMaterialInformation material) {
		double amount = material.getQuantityNeededToRepair();
		synchronized (fRepairMaterials) {
			Double materialAmount = fRepairMaterials.get(material.getName());
			
			if (materialAmount == null) {
				fRepairMaterials.put(material.getName(), amount);
			} else {
				fRepairMaterials.put(material.getName(), materialAmount.doubleValue() + amount);
			}
		}
	}
	

	public String toString() {
		StringBuilder str = new StringBuilder();
		
		str.append("Statistics:" + EOL);
		str.append("Total money Gained: " + fMoneyGained.toString() + EOL);
		str.append("Completed rental requests: " + fRentalRequests.toString() + EOL);
		str.append("Used Tools: " + fRepairTools.toString() + EOL);
		str.append("Used Materials: " + fRepairMaterials.toString() + EOL);
		
		return str.toString();
	}

}