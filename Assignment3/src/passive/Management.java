package passive;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import active.RunnableClerk;
import active.RunnableCustomerGroupManager;
import active.RunnableMaintenanceRequest;
import structures.Asset;
import structures.Assets;
import structures.ClerkDetails;
import structures.CustomerGroupDetails;
import structures.DamageReport;
import structures.RentalRequest;
import structures.RepairMaterialInformation;
import structures.RepairToolInformation;
import structures.Warehouse;

/**
 * The Class Management.
 */
public class Management {
	private static final Logger log = Logger.getLogger(Management.class.getName());
	public static final double SIM_TIME_FACTOR = 1;
	
	private List<ClerkDetails> 				fClerks;
	private List<CustomerGroupDetails> 		fCustomerGroups;
	private BlockingQueue<RentalRequest> 	fRentalRequestsQueue;
	private BlockingQueue<DamageReport> 	fDamageReportsQueue;
	private Assets				 			fAssets;	
	private Warehouse 						fWarehouse;
	private AtomicInteger 					fNumberOfRentalRequests;
	private int 							fNumberOfMaintenancePersons;
	private CyclicBarrier 					fDayFinished;
	private boolean 						fShouldStop;
	private Statistics						fStatistics;
	private AtomicInteger 					fDamageReportsToBeReceived;
	
	private HashMap<String, List<RepairToolInformation>> 	 fToolsInfo;
	private HashMap<String, List<RepairMaterialInformation>> fMaterialsInfo;
	
	/**
	 * Instantiates a new management object.
	 *
	 * @param warehouse the warehouse
	 * @param numberOfRentalRequests the number of rental requests
	 * @param numberOfMaintenancePersons the number of maintenance persons
	 */
	public Management(Warehouse warehouse, int numberOfRentalRequests, int numberOfMaintenancePersons) {
		log.info("Management CTOR"); 
		fNumberOfRentalRequests = new AtomicInteger(numberOfRentalRequests);
		fWarehouse = warehouse;
		fNumberOfMaintenancePersons = numberOfMaintenancePersons;
		fRentalRequestsQueue = new LinkedBlockingQueue<RentalRequest>();
		fDamageReportsQueue = new LinkedBlockingQueue<DamageReport>();
		fToolsInfo = new HashMap<String, List<RepairToolInformation>>();
		fMaterialsInfo = new HashMap<String, List<RepairMaterialInformation>>();
		fStatistics = new Statistics();
		fShouldStop = false;
		fDamageReportsToBeReceived = new AtomicInteger(0);
	}
	
	/**
	 * Initiates the clerks.
	 *
	 * @param clerks the clerks
	 */
	public void initiateClerks(List<ClerkDetails> clerks) {
		log.info("initiate clerks");
		fClerks = clerks;
	}
	
	/**
	 * Initiates the customer groups.
	 *
	 * @param customerGroups the customer groups
	 * @throws Exception the exception
	 */
	public void initiateCustomerGroups(List<CustomerGroupDetails> customerGroups) throws Exception {
		log.info("initiate CustomerGroups");
		fCustomerGroups = customerGroups;
	}
	
	/**
	 * Initiates the assets.
	 *
	 * @param assets the assets
	 */
	public void initiateAssets(Assets assets) {
		log.info("initiate Assets");
		fAssets = assets;
	}
	
	/**
	 * Adds the item repair tools information.
	 *
	 * @param item the item
	 * @param tools the tools
	 */
	public void addItemRepairToolsInformation(String item, List<RepairToolInformation> tools) {
		log.info("Add ItemRepairToolsInformation for: " + item);
		fToolsInfo.put(item, tools);
	}
	
	/**
	 * Adds the item repair materials information.
	 *
	 * @param item the item
	 * @param materials the materials
	 */
	public void addItemRepairMaterialsInformation(String item, List<RepairMaterialInformation> materials) {
		log.info("Add ItemRepairMaterialsInformation for: " + item);
		fMaterialsInfo.put(item, materials);
	}
	
	/**
	 * Adds the rental request.
	 *
	 * @param rentalRequest the rental request
	 * @throws InterruptedException the interrupted exception
	 */
	public void addRentalRequest(RentalRequest rentalRequest) throws InterruptedException {
		log.info("Added rental request.");
		fRentalRequestsQueue.put(rentalRequest);
	}
	
	/**
	 * Find an asset according to given type and size.
	 *
	 * @param type the type
	 * @param size the size
	 * @return the asset
	 * @throws InterruptedException the interrupted exception
	 */
	public Asset findAsset(String type, int size) throws InterruptedException {
		return fAssets.find(type, size);
	}

	/**
	 * Notify others when an asset could be free.
	 */
	public void notifyFreeAsset() {
		synchronized (fAssets) {
			fAssets.notifyAll();
		}
	}
	
	/**
	 * Report shift finished.
	 *
	 * @throws InterruptedException the interrupted exception
	 * @throws BrokenBarrierException the broken barrier exception
	 */
	public void reportShiftFinished() throws InterruptedException, BrokenBarrierException {
		fDayFinished.await();
	}
	
	/**
	 * Adds the damage report to the queue.
	 *
	 * @param damageReport the damage report
	 * @throws InterruptedException the interrupted exception
	 */
	public void addDamageReport(DamageReport damageReport) throws InterruptedException {
		// Apply damage
		damageReport.applyDamage();
		
		// Add to the damage reports queue
		fDamageReportsQueue.put(damageReport);
		
		// Decrement counter & notify
		fDamageReportsToBeReceived.decrementAndGet();
		synchronized (fDamageReportsToBeReceived) {
			fDamageReportsToBeReceived.notifyAll();
		}
	}
	
	/**
	 * Adds the money gained statistic.
	 *
	 * @param amount the amount
	 */
	public void addMoneyGainedStatistic(double amount) {
		fStatistics.addMoneyGained(amount);
	}
	
	/**
	 * Adds the rental request statistic.
	 *
	 * @param rentalRequest the rental request
	 */
	public void addRentalRequestStatistic(RentalRequest rentalRequest) {
		fStatistics.addCompletedRentalRequest(rentalRequest);
	}
	
	/**
	 * Adds the used tool statistic.
	 *
	 * @param tool the tool
	 */
	public void addUsedToolStatistic(RepairToolInformation tool) {
		fStatistics.addUsedTool(tool);
	}
	
	/**
	 * Adds the consumed material statistic.
	 *
	 * @param material the material
	 */
	public void addConsumedMaterialStatistic(RepairMaterialInformation material) {
		fStatistics.addConsumedMaterial(material);
	}
	
	/**
	 * Checks if the simulation should stop
	 *
	 * @return true, if the simulation should stop
	 */
	public boolean shouldStop() {
		return fShouldStop;
	}

    /**
     * Gets the tool information.
     *
     * @param assetContentName the asset content name
     * @return the tool information
     */
    public List<RepairToolInformation> getToolInformation(String assetContentName){
        return fToolsInfo.get(assetContentName);
    }

    /**
     * Gets the material information.
     *
     * @param assetContentName the asset content name
     * @return the material information
     */
    public List<RepairMaterialInformation> getMaterialInformation(String assetContentName){
        return fMaterialsInfo.get(assetContentName);
    }
    
    /**
     * Adds wait for damage report.
     */
    public void addWaitForDamageReport() {
    	fDamageReportsToBeReceived.incrementAndGet();
    }
    
    private void waitForDamageReports() throws InterruptedException {
    	synchronized (fDamageReportsToBeReceived) {
    		while (fDamageReportsToBeReceived.get() > 0) {
    			fDamageReportsToBeReceived.wait();
			}
    	}
    }
	
	/**
	 * Start simulation of real estate investment trust.
	 *
	 * @throws Exception the exception
	 */
	public void startSimulation() throws Exception {
		fDayFinished = new CyclicBarrier(fClerks.size() + 1);	
		
		// Run Clerks...
		for (ClerkDetails clerk : fClerks) {
			(new Thread(new RunnableClerk(clerk, fRentalRequestsQueue, fNumberOfRentalRequests, this))).start();
		}
		
		// Run groups...
		for (CustomerGroupDetails customerGroup : fCustomerGroups) {
			(new Thread(new RunnableCustomerGroupManager(customerGroup, this))).start();
		}
        
		while (!shouldStop()) {
			log.info("Waiting for all clerks to finish their shift.");
			fDayFinished.await();
			
			log.info("MGMT: Clerks finished their shift, waiting for damage reports.");
			// the clerks finished their shift and are waiting... 
			
			// Waiting for all damage reports to be received
			waitForDamageReports();
			
			log.info("MGMT: Got all damage reports, running maintenance people.");
			
            // Run maintenance people:
			// initiate new thread pool
			ExecutorService maintenanceExecutor = Executors.newFixedThreadPool(fNumberOfMaintenancePersons);
			
            int id = 1;
            // While the reportsQueue is not empty, run maintenance request
            while (!fDamageReportsQueue.isEmpty()) {
            	DamageReport report = fDamageReportsQueue.poll();
            	maintenanceExecutor.submit(new RunnableMaintenanceRequest(fWarehouse, report, this, id));
            	id++;
            }
            
			// shutdown maintenance thread pool
			maintenanceExecutor.shutdown();
			
            //wait for all maintenance threads to finish
            maintenanceExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
			
			if (fNumberOfRentalRequests.get() <= 0) {
				fShouldStop = true;
			}
			
			// notify the clerks to continue
			log.info("============ MGMT: Starting a new day. ============");
			synchronized (this) {
				notifyAll();
			}
		}
		
		// Print Statistics
		System.out.println(fStatistics.toString());
	}
}
