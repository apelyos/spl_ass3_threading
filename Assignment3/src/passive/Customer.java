package passive;

import java.util.Random;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class Represents objects of type Customer.
 */
@XmlRootElement(name = "customer")
@XmlAccessorType(XmlAccessType.FIELD)
public class Customer {
	private final double REGULAR_WEAR_AND_TEAR = 0.5;
	
	/**
	 * The Enum VandalismType.
	 */
	@XmlEnum
	private static enum VandalismType {
		@XmlEnumValue("Arbitrary")
		ARBITRARY, 
		@XmlEnumValue("Fixed")
		FIXED,
		@XmlEnumValue("None")
		NONE};
	
	@XmlElement(name = "name")
	private String fName;
	
	@XmlElement(name = "vandalism")
	private VandalismType fVandalismType;
	
	@XmlElement(name = "minimumdamage")
	private int fMinDamage;
	
	@XmlElement(name = "maximumdamage")
	private int fMaxDamage;
	
	/**
	 * Calculates the damage done by the customer.
	 *
	 * @return damage
	 */
	public double doDamage() {
		if (fVandalismType == VandalismType.ARBITRARY) {
			Random rand = new Random();
			return fMinDamage + (rand.nextDouble() * (fMaxDamage - fMinDamage));
		} else if (fVandalismType == VandalismType.FIXED) {
			return ((fMinDamage + fMaxDamage) / 2);
		} else {
			return REGULAR_WEAR_AND_TEAR;
		}
	}
}
