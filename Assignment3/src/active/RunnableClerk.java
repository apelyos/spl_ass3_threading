package active;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import passive.Management;
import structures.Asset;
import structures.ClerkDetails;
import structures.RentalRequest;

/**
 * The Class RunnableClerk.
 */
public class RunnableClerk implements Runnable{
	private static final Logger log = Logger.getLogger(RunnableClerk.class.getName());
	private ClerkDetails 					fClerkDetails;
	private BlockingQueue<RentalRequest> 	fRentalRequestsQueue;
	private AtomicInteger 					fNumberOfRentalRequests;
	private Management 						fManagement;
	
	private final int MILLISECONDS_TO_SECONDS = 1000;
	private final int ROUND_TRIP = 2;
	private final int MAX_SHIFT_TIME = 8;
	
	/**
	 * Instantiates a new runnable clerk.
	 *
	 * @param clerkDetails the clerk details
	 * @param rentalRequestsQueue the rental requests queue
	 * @param numberOfRentalRequests the number of rental requests
	 * @param management the management
	 */
	public RunnableClerk(ClerkDetails clerkDetails, BlockingQueue<RentalRequest> rentalRequestsQueue, 
			AtomicInteger numberOfRentalRequests, Management management) {
		fClerkDetails = clerkDetails;
		fRentalRequestsQueue = rentalRequestsQueue;
		fNumberOfRentalRequests = numberOfRentalRequests;
		fManagement = management;
	}
	
	/**
	 * Runs the clerk routine as described in section 4.1 in the assignment PDF.
	 *
	 */
	@Override
	public void run() {
		log.info("Start: Clerk thread id: " + Thread.currentThread().getId());
		long totalShiftTime = 0;
		
		try {
			while (!fManagement.shouldStop()) {

				if (fNumberOfRentalRequests.decrementAndGet() < 0) {
					log.info("Got no more rental requests in the common queue, skipping shift. tid: " + Thread.currentThread().getId());
					fManagement.reportShiftFinished();
					
					waitForNewDay();
					
				} else { 
					// Dequeue rental request from the common queue (also decrement the total number)
					log.info("Clerk: Dequeue rental request");
					RentalRequest request = fRentalRequestsQueue.take(); 
					
					// Tell the management to wait for a damage report to be received later
					fManagement.addWaitForDamageReport();
					
					// Find suitable asset & mark booked (we could wait here)
					log.info("Trying to find asset and book it. Contention may be here. tid: " + Thread.currentThread().getId());
					Asset asset = fManagement.findAsset(request.getAssetType(), request.getAssetSize());
					
					// Mark as booked (waiting to be available) Fulfill request
					request.assignAsset(asset);
					
					// Simulating going to the asset & sleeping 
					totalShiftTime = totalShiftTime + simulateGoToAsset(asset);
					
					log.info("Went to asset. Done sleeping. Notifying customer group. tid: " + Thread.currentThread().getId());
					// Asset is ready for customer group, notify them
					synchronized (request) {
						request.notifyAll();
					}
					
					if (totalShiftTime > MAX_SHIFT_TIME) {
						log.info("Done with this shift (MAX_SHIFT_TIME reached). tid: " + Thread.currentThread().getId());
						// Probably going to wait here
						totalShiftTime = 0;
						fManagement.reportShiftFinished();
						
						waitForNewDay();
					}
				}
			}
			
			log.info("Got a stop command. Exiting thread. tid: " + Thread.currentThread().getId());
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
	}

	private void waitForNewDay() throws InterruptedException {
		log.info("Waiting for a new day to start. (after cycle barrier), tid: " + Thread.currentThread().getId());
		synchronized (fManagement) {
			fManagement.wait();
		}
	}
	
	private long simulateGoToAsset(Asset asset) throws InterruptedException {
		long sleepTime = Math.round(fClerkDetails.getLocation().calculateDistance(asset.getLocation()) * ROUND_TRIP);
		log.info("Going to asset and sleeping for " + sleepTime + " seconds.  tid: " + Thread.currentThread().getId());
		Thread.sleep(Math.round(sleepTime * MILLISECONDS_TO_SECONDS * Management.SIM_TIME_FACTOR));
		return sleepTime;
	}
	
}
