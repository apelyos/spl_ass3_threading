package active;

import java.util.concurrent.Callable;
import java.util.logging.Logger;

import passive.Customer;
import passive.Management;
import structures.Asset;

/**
 * The Class CallableSimulateStayInAsset.
 */
class CallableSimulateStayInAsset implements Callable<Double> {
	private static final Logger log = Logger.getLogger(CallableSimulateStayInAsset.class.getName());
	private final int MILLISECONDS_TO_SECONDS = 1000;
	private final int HOURS_IN_DAY = 24;
	private Asset fAsset;
	private Customer fCustomer;
	private int fDurationInDays;
	
	/**
	 * Instantiates a new callable simulate stay in asset.
	 *
	 * @param asset the asset
	 * @param customer the customer
	 * @param durationInDays the duration in days
	 */
	CallableSimulateStayInAsset(Asset asset, Customer customer, int durationInDays) {
		fAsset = asset;
		fCustomer = customer;
		fDurationInDays = durationInDays;
	}
	
	/**
	 * Calling simulate stay in asset.
	 * 
	 * @return damage done to the asset by a single customer
	 **/
	@Override
	public Double call() throws Exception {
		log.info("Simulating stay, nDays: " + fDurationInDays + ", tid: " + Thread.currentThread().getId());
		
		Thread.sleep(Math.round(fDurationInDays * HOURS_IN_DAY * MILLISECONDS_TO_SECONDS * Management.SIM_TIME_FACTOR));
		
		return fCustomer.doDamage(); //returns the damage
	}
	
}
