package active;

import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import passive.Customer;
import passive.Management;
import structures.CustomerGroupDetails;
import structures.DamageReport;
import structures.RentalRequest;

/**
 * The Class RunnableCustomerGroupManager.
 */
public class RunnableCustomerGroupManager implements Runnable{
	private static final Logger log = Logger.getLogger(RunnableCustomerGroupManager.class.getName());
	private CustomerGroupDetails fCustomerGroupDetails;
	private Management fManagement; 
	
	/**
	 * Instantiates a new runnable customer group manager.
	 *
	 * @param customerGroupDetails the customer group details
	 * @param management the management
	 */
	public RunnableCustomerGroupManager(CustomerGroupDetails customerGroupDetails, Management management) {
		fCustomerGroupDetails = customerGroupDetails;
		fManagement = management;
	}

	/**
	 * Runs the CustomerGroupManager routine as described in section 4.2 in the assignment PDF.
	 *
	 */
	@Override
	public void run() {
		log.info("Start: Customer group thread tid: " + Thread.currentThread().getId());
		
		RentalRequest rentalRequest; 
		
		// Get rental request from the group details
		rentalRequest = fCustomerGroupDetails.pollRentalRequest();
		
		// Loop until there are no more rental requests
		try {
			while (rentalRequest != null) {
				log.info("Sending a rental request to MGMT.");
				fManagement.addRentalRequest(rentalRequest);
				
				log.info("Waiting for the request to be fulfilled. tid: " + Thread.currentThread().getId());
				synchronized (rentalRequest) {
					rentalRequest.wait();
				}
				
				log.info("Woke Up, calling CallableSimulateStayInAsset and waiting. tid: " + Thread.currentThread().getId());
				
				rentalRequest.occupyAsset();
				
				// call callableStayInAsset
				
				ExecutorService  pool = Executors.newCachedThreadPool();
			    CompletionService<Double> ecs = new ExecutorCompletionService<Double>(pool);
			    List<Customer> customers = fCustomerGroupDetails.getCustomers();
			    
			    for (Customer customer : customers) {
			    	ecs.submit(new CallableSimulateStayInAsset(rentalRequest.getAssignedAsset(), 
			    			customer, rentalRequest.getDurationOfStay()));
			    }
			    
			    double combinedDamage = 0.0;
			    	
			    for (int i = 0; i < customers.size(); ++i) {
			        Double damagePercent = ecs.take().get(); 
			        if (damagePercent != null) {
			        	combinedDamage += damagePercent.doubleValue();
			        }
			    }
				
			    pool.shutdown();
				
				// Generates a DamageReport & send to  management (the management updates damage)
				fManagement.addDamageReport(new DamageReport(rentalRequest.getAssignedAsset(), combinedDamage));
				
				// Add statistics
				fManagement.addRentalRequestStatistic(rentalRequest);
				fManagement.addMoneyGainedStatistic(rentalRequest.getAssignedAsset().calculateCost(customers.size(), rentalRequest.getDurationOfStay()));
				
				// Free the asset in the end & notify
			    log.info("Asset was freed. notifying. tid: " + Thread.currentThread().getId());
				rentalRequest.availableAsset();
				fManagement.notifyFreeAsset();
				
				// Get next rental request
				rentalRequest = fCustomerGroupDetails.pollRentalRequest();
			}
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		log.info("Got no more rental requests in the group. Exiting Thread. tid: " + Thread.currentThread().getId());
	}
	
}
