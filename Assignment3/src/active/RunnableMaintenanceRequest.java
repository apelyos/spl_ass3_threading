package active;

import passive.Management;
import structures.Asset;
import structures.AssetContent;
import structures.DamageReport;
import structures.RepairMaterialInformation;
import structures.RepairToolInformation;
import structures.Warehouse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * The Class RunnableMaintenanceRequest.
 */
public class RunnableMaintenanceRequest implements Runnable{
    private static final Logger log = Logger.getLogger(RunnableMaintenanceRequest.class.getName());
    private Warehouse fWarehouse;
    private Asset fAsset;
    private Management fManagement;
    private Map<String, RepairToolInformation> fTools;
    private List<RepairMaterialInformation> fMaterial;
    private int fId;

    /**
     * Instantiates a new RunnableMaintenanceRequest object
     * @param warehouse - the warehouse
     * @param report - the report to handle
     * @param management - the management
     * @param id - number of report handled
     */
    public RunnableMaintenanceRequest(Warehouse warehouse, DamageReport report, Management management, int id){
    	fTools = new HashMap<String, RepairToolInformation>();
    	fMaterial = new ArrayList<RepairMaterialInformation>();
        fWarehouse = warehouse;
        fAsset = report.getAsset();
        fManagement = management;
        fId = id;
    }

    /**
     * Gathering all tools and materials from the Asset's AssetContents.
     * For tools taking the max of each tool needed, for material taking the sum.
     */
    private void gatherToolsAndMaterialsNeeded() {
        for (AssetContent assetContent : fAsset.getAssetContent()){
            for (RepairMaterialInformation material : fManagement.getMaterialInformation(assetContent.getName())){
                fMaterial.add(material);
            }
            
            //gathering tools info - taking max of each tool
            for (RepairToolInformation tool : fManagement.getToolInformation(assetContent.getName())){
                if (fTools.containsKey(tool.getName())){
                    RepairToolInformation existingTool = fTools.get(tool.getName());
                    if (existingTool.getQuantityNeededToRepair() < tool.getQuantityNeededToRepair()){
                        fTools.remove(existingTool.getName());
                        fTools.put(tool.getName(), tool);
                    }
                } else {
                    fTools.put(tool.getName(), tool);
                }
            }
        }
    }

    /**
     * Calculating the repair cost.
     *
     * @return the repair cost
     */
    private long calculateCost(){
        double repairCost = 0;
        for (AssetContent assetContent : fAsset.getAssetContent()) {
            repairCost += fAsset.getDamagePercent() * assetContent.getRepairCostMultiplier();
        }
        return Math.round(repairCost);
    }

	/**
	 * Runs the MaintenanceRequest routine as described in section 4.3 in the assignment PDF.
	 *
	 */
    @Override
    public void run() {
    	log.info("Maintenance request #" + fId + " Started");
    	
    	// lock the specific asset for fixing
    	synchronized (fAsset) {
	        if(!fAsset.isDamaged()){
	            log.info("Maintenance request #" + fId + "- asset is not damaged. Exiting.");
	            return;
	        }
	        
	        // Gathering information
	        gatherToolsAndMaterialsNeeded();
	        
	        log.info("Acquiring tools...");
	        acquireTools();
	        consumeMaterials();
	
	        try {
	            log.info("Maintenance request #" + fId + " sleeping for: " + calculateCost());
	            Thread.sleep(Math.round(calculateCost() * Management.SIM_TIME_FACTOR));
	        } catch (InterruptedException e) {
	        	log.severe(e.getMessage());
	        }
	        
	        log.info("Releasing tools.");
	        releaseTools();
	        
	        // Mark asset as fixed & notify
	        log.info("Marking asset as fixed & notifying.");
	        fAsset.assetFixed();
	        fManagement.notifyFreeAsset();
    	}

        log.info("Maintenance request #" + fId + " Finished. Exiting.");
    }

    /**
     * Consume materials needed for fixing.
     */
    private void consumeMaterials() {
        for (RepairMaterialInformation material : fMaterial){
            fWarehouse.consumeMaterial(material.getName(), material.getQuantityNeededToRepair());
            //adding to statistics:
            fManagement.addConsumedMaterialStatistic(material);
        }
    }

    /**
     * Acquire tools needed for fixing.
     */
    private void acquireTools() {
        for(String toolName : fetchSortedTools()){
            RepairToolInformation tool = fTools.get(toolName);
            fWarehouse.acquireTool(toolName, tool.getQuantityNeededToRepair());
            fManagement.addUsedToolStatistic(tool);
        }
    }

    /**
     * Sorting the tools in order to prevent a deadlock because of a maintenance request dependency.
     *
     * @return sorted tools.
     */
    private List<String> fetchSortedTools() {
        List<String> sortedTools = new ArrayList<String>(fTools.keySet()); 
        Collections.sort(sortedTools);
        return sortedTools;
    }

    /**
     * Releasing tools after the maintenance is done.
     */
    private void releaseTools() {
        for(String toolName : fetchSortedTools()){
            fWarehouse.releaseTool(toolName, fTools.get(toolName).getQuantityNeededToRepair());
        }
    }
}
