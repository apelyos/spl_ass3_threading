package structures;

import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class Asset.
 */
@XmlRootElement(name = "asset")
@XmlAccessorType(XmlAccessType.FIELD)
public class Asset {
	private static final Logger log = Logger.getLogger(Asset.class.getName());
	
	// Constants
	private static enum Status { AVAILABLE, BOOKED, OCCUPIED, UNAVAILABLE };
	private final double FULL_HEALTH = 100.0;
	private final double DAMAGE_THRESHOLD = 65.0;
	
	// Instance Variables
	@XmlElement(name = "type")
	private String fType;
	
	@XmlElement(name = "location")
	private Location fLocation;
	
	@XmlElementWrapper(name = "assetcontents")
	@XmlElement(name = "assetcontent")
	private List<AssetContent> fAssetContent;
	
	@XmlElement(name = "costpernight")
	private double fCostPerNight;
	
	@XmlElement(name = "size")
	private int fSize;

    private double fHealth;

	private Status fStatus;
	
	private Asset() {
		// Private constructor for JAXB
		fStatus = Status.AVAILABLE;
		fHealth = FULL_HEALTH;
	}
	
	/**
	 * Gets the asset type.
	 *
	 * @return the type
	 */
	final String getType() {
		return new String(fType);
	}
	
	/**
	 * Gets the asset size.
	 *
	 * @return the size
	 */
	final int getSize() {
		return fSize;
	}
	
	/**
	 * Gets the asset location.
	 *
	 * @return the location
	 */
	public Location getLocation() {
		return fLocation;
	}
	
	/**
	 * Checks if is available.
	 *
	 * @return true, if is available
	 */
	public boolean isAvailable() {
		return (fStatus == Status.AVAILABLE);
	}
	
	/**
	 * Mark asset as Booked.
	 */
	public synchronized void bookAsset() {
		log.info("Marking asset as booked.");
		fStatus = Status.BOOKED;
	}
	
	/**
	 * Mark asset as Available.
	 */
	public synchronized void availableAsset() {
		if (isDamaged()) {
			log.info("Asset is damaged, can't make it available.");
			return;
		}
		
		// if damage, do not make available
		log.info("Marking asset as available.");
		fStatus = Status.AVAILABLE;
	}
	
	/**
	 * Mark asset as Occupy.
	 */
	public synchronized void occupyAsset() {
		log.info("Marking asset as occupied.");
		fStatus = Status.OCCUPIED;
	}
	
	/**
	 * Mark asset as Damaged.
	 *
	 * @param amount the amount
	 */
	public synchronized void damageAsset(double amount) {
		fHealth -= amount;
		
		log.info("Damaging asset by: " + amount + ". Current health: " + fHealth);
		
		// if the current health level is below 65, mark as damaged
		if (isDamaged()){
			log.info("Asset is damaged, Marking asset as unavailable.");
			fStatus = Status.UNAVAILABLE;
		}
	}
	
	/**
	 * Checks if is damaged.
	 *
	 * @return true, if is damaged
	 */
	public boolean isDamaged() {
		return (fHealth < DAMAGE_THRESHOLD);
	}
	
	/**
	 * Calculate cost.
	 *
	 * @param numOfPeople the num of people
	 * @param numOfNights the num of nights
	 * @return the cost
	 */
	public double calculateCost(int numOfPeople, int numOfNights) {
		return numOfPeople * numOfNights * fCostPerNight;
	}

    /**
     * Gets the asset content.
     *
     * @return the asset content
     */
    public List<AssetContent> getAssetContent(){
        return fAssetContent;
    }

    /**
     * Mark asset as fixed.
     */
    public void assetFixed(){
    	// Mark asset as fixed
        fHealth = FULL_HEALTH;
        fStatus = Status.AVAILABLE;
    }
    
    /**
     * Gets the damage percent.
     *
     * @return the damage percent
     */
    public double getDamagePercent() {
    	if (fHealth < 0) {
    		return 0;
    	}
    	
    	return fHealth;
    }

}
