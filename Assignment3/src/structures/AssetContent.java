package structures;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class AssetContent.
 */
@XmlRootElement(name = "assetcontent")
@XmlAccessorType(XmlAccessType.FIELD)
public class AssetContent {
	@XmlElement(name = "name")
	private String fName;
	
	@XmlElement(name = "repairmultiplier")
	private double fRepairCostMultiplier;
	
	private AssetContent() {
		// Default constructor for JAXB
	}

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName(){
        return fName;
    }

    /**
     * Gets the repair cost multiplier.
     *
     * @return the repair cost multiplier
     */
    public double getRepairCostMultiplier(){
        return fRepairCostMultiplier;
    }
}
