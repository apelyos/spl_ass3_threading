package structures;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class RepairToolInformation.
 */
@XmlRootElement(name = "tool")
@XmlAccessorType(XmlAccessType.FIELD)
public class RepairToolInformation {
	@XmlElement(name = "name")
	private String fName;
	
	@XmlElement(name = "quantity")
	private int fQuantityNeededToRepair;
	
	private RepairToolInformation() {
		// Empty constructor for JAXB
	}

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return fName;
    }

    /**
     * Gets the quantity needed to repair.
     *
     * @return the quantity needed to repair
     */
    public int getQuantityNeededToRepair() {
        return fQuantityNeededToRepair;
    }
}
