package structures;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

// TODO: Auto-generated Javadoc
/**
 * The Class RepairMaterial.
 */
@XmlRootElement(name = "material")
@XmlAccessorType(XmlAccessType.FIELD)
public class RepairMaterial {
	@XmlElement(name = "name")
	private String fName;
	
	@XmlElement(name = "quantity")
	private double fQuantityInWarehouse;
	
	private RepairMaterial() {
		// Empty constructor for JAXB
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return new String(fName);
	}
	
	/**
	 * Consume this material.
	 *
	 * @param amount the amount
	 * @throws Exception the exception
	 */
	synchronized void consume(double amount) throws Exception {
		if (fQuantityInWarehouse < amount) {
			throw new Exception("Not enough material left of kind: " + fName);
		}
		
		fQuantityInWarehouse = fQuantityInWarehouse - amount;
	}
	
}
