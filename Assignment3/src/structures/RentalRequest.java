package structures;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class RentalRequest.
 */
@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
public class RentalRequest {
	
	/**
	 * The Enum RequestStatus.
	 */
	private static enum RequestStatus { INCOMPLETE, FULFILLED, INPROGRESS, COMPLETE };
	
	public final String DELIMITER = ", ";
	
	@XmlAttribute(name = "id")
	private int fId;
	
	@XmlElement(name = "type")
	private String fAssetType;
	
	@XmlElement(name = "size")
	private int fAssetSize;
	
	@XmlElement(name = "duration")
	private int fDurationOfStayInDays;
	
	private Asset fAsset;
	private RequestStatus fRequestStatus;
	
	private RentalRequest() {
		// Default constructor for JAXB
		fRequestStatus = RequestStatus.INCOMPLETE;
	}
	
	/**
	 * Gets the needed asset type.
	 *
	 * @return the asset type
	 */
	public String getAssetType() {
		return new String(fAssetType);
	}
	
	/**
	 * Gets the needed asset size.
	 *
	 * @return the asset size
	 */
	public int getAssetSize() {
		return fAssetSize;
	}
	
	/**
	 * Gets the assigned asset.
	 *
	 * @return the assigned asset
	 */
	public Asset getAssignedAsset() {
		return fAsset;
	}
	
	/**
	 * Gets the duration of stay.
	 *
	 * @return the duration of stay
	 */
	public int getDurationOfStay() {
		return fDurationOfStayInDays;
	}
	
	/**
	 * Assign asset to this request.
	 *
	 * @param asset the asset
	 */
	public void assignAsset(Asset asset) {
		fRequestStatus = RequestStatus.FULFILLED;
		fAsset = asset;
	}
	
	/**
	 * Mark the asset as Occupy.
	 */
	public void occupyAsset() {
		fRequestStatus = RequestStatus.INPROGRESS;
		fAsset.occupyAsset();
	}
	
	/**
	 * Mark the asset as available.
	 */
	public void availableAsset() {
		fRequestStatus = RequestStatus.COMPLETE;
		fAsset.availableAsset();
	}
	
	public String toString() {
		StringBuilder str = new StringBuilder();
		
		str.append("[ID=" + fId + DELIMITER);
		str.append("Type=" + fAssetType + DELIMITER);
		str.append("Size=" + fAssetSize + DELIMITER);
		str.append("DurationOfStay=" + fDurationOfStayInDays +  DELIMITER);
		str.append("RequestStatus=" + fRequestStatus + "]");
				
		return str.toString();
	}
	
}
