package structures;
import parsing.RepairMaterialsMapAdapter;
import parsing.RepairToolsMapAdapter;

import java.util.Map;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The Class Warehouse.
 */
@XmlRootElement(name = "warehouse")
@XmlAccessorType(XmlAccessType.FIELD)
public class Warehouse  {
    private static final Logger log = Logger.getLogger(Warehouse.class.getName());
    
	@XmlElement(name = "tools")
	@XmlJavaTypeAdapter(RepairToolsMapAdapter.class)
	private Map<String, RepairTool> fTools;
	
	@XmlElement(name = "materials")
	@XmlJavaTypeAdapter(RepairMaterialsMapAdapter.class)
	private Map<String, RepairMaterial>  fMaterials;

	private Warehouse() {
		// leave empty for JAXB
	}

	/**
	 * Acquire tool from the warehouse.
	 *
	 * @param toolName the tool name
	 * @param quantity the quantity
	 */
	public void acquireTool(String toolName, int quantity) {
        try {
            fTools.get(toolName).acquire(quantity);
            log.info(quantity + " " + toolName + " acquired");
        } catch (Exception e) {
        	log.info(e.getMessage());
        }
	}

	/**
	 * Consume material from the warehouse.
	 *
	 * @param materialName the material name
	 * @param amount the amount
	 */
	public void consumeMaterial(String materialName, double amount) {
        try {
            fMaterials.get(materialName).consume(amount);
            log.info(amount + " " + materialName + " consumed");
        } catch (Exception e) {
            log.info(e.getMessage());
        }
	}

    /**
     * Release tool to the warehouse.
     *
     * @param toolName the tool name
     * @param quantity the quantity
     */
    public void releaseTool(String toolName, int quantity){
        fTools.get(toolName).release(quantity);
        log.info(quantity + " " + toolName + " released");
    }
}
