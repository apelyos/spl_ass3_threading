package structures;
import java.util.concurrent.Semaphore;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import parsing.IntegerSemaphoreAdapter;

/**
 * The Class RepairTool.
 */
@XmlRootElement(name = "tool")
@XmlAccessorType(XmlAccessType.FIELD)
public class RepairTool {
	@XmlElement(name = "name")
	private String fName;
	
	@XmlElement(name = "quantity")
	@XmlJavaTypeAdapter(IntegerSemaphoreAdapter.class)
	private Semaphore fQuantityInWarehouse;
	
	private RepairTool() {
		// Empty constructor for JAXB
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return new String(fName);
	}
	
	/**
	 * Acquire the tool.
	 *
	 * @param amount the amount
	 * @throws Exception the exception
	 */
	void acquire(int amount) throws Exception {
		fQuantityInWarehouse.acquire(amount);
	}
	
	/**
	 * Release the tool.
	 *
	 * @param amount the amount
	 */
	void release(int amount) {
		fQuantityInWarehouse.release(amount);
	}

    public int getQuantity(){
        return fQuantityInWarehouse.availablePermits();
    }
}
