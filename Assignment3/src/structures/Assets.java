package structures;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The Class Assets.
 */
@XmlRootElement(name = "assets")
@XmlAccessorType(XmlAccessType.FIELD)
public class Assets {
	private static final Logger log = Logger.getLogger(Assets.class.getName());
	
	@XmlElement(name = "asset")
	private List<Asset> fAssets;
	
	private Assets() {
		// Empty constructor for JAXB
	}
	
	/**
	 * Find an asset according to given type and size.
	 *
	 * @param type the type
	 * @param size the size
	 * @return the asset
	 * @throws InterruptedException the interrupted exception
	 */
	public synchronized Asset find(String type, int size) throws InterruptedException {
		log.info("Find asset start, type: " + type + ", size: " + size  + ", tid: " + Thread.currentThread().getId());
		Asset assetToReturn = null;

		// While we didn't find any good asset to book
		while (assetToReturn == null) {
			int minSizeFound = Integer.MAX_VALUE;
			
			// Iterate all the asset
			for (Asset asset : fAssets) {
				if (asset.getType().equals(type) && asset.getSize() >= size && asset.isAvailable()) {
					if (asset.getSize() < minSizeFound) { // find the asset with the closest size
						assetToReturn = asset;
						minSizeFound = asset.getSize();
					}
				}
			}
			
			if (assetToReturn == null) {
				log.info("Waiting for an asset to become avilable. tid: " + Thread.currentThread().getId());
				this.wait();
			}
		}
		
		log.info("Found an asset, book & return it. tid: " + Thread.currentThread().getId());
		assetToReturn.bookAsset();
		return assetToReturn; 
	}
	
	/**
	 * Retrieve damaged assets.
	 *
	 * @return the list
	 */
	public synchronized List<Asset> retrieveDamagedAssets() {
		List<Asset> damagedAssets = new ArrayList<Asset>();
		for (Asset asset : fAssets){
			if (asset.isDamaged()){
				damagedAssets.add(asset);
			}
		}

		return damagedAssets;
	}
}

