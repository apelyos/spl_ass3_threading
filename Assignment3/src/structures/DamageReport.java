package structures;

/**
 * The Class DamageReport.
 */
public class DamageReport {
	private Asset fAsset;
	private double fDamagePercent;

    /**
     * Gets the asset.
     *
     * @return the asset
     */
    public Asset getAsset() {
        return fAsset;
    }

    /**
     * Gets the damage percent.
     *
     * @return the damage percent
     */
    public double getDamagePercent() {
        return fDamagePercent;
    }

    /**
     * Instantiates a new damage report.
     *
     * @param asset the asset
     * @param damagePercent the damage percent
     */
    public DamageReport(Asset asset, double damagePercent) {
		fAsset = asset;
		fDamagePercent = damagePercent;
	}
	
	/**
	 * Apply damage to the asset.
	 */
	public void applyDamage() {
		fAsset.damageAsset(fDamagePercent);
	}
}
