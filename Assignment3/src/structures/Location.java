package structures;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class Location.
 */
@XmlRootElement(name = "Location")
@XmlAccessorType(XmlAccessType.FIELD)
public class Location {
	@XmlAttribute(name = "x")
	private double fX;
	
	@XmlAttribute(name = "y")
	private double fY;
	
	@SuppressWarnings("unused")
	private Location() {
		// Empty constructor for JAXB
	}
	
	/**
	 * Instantiates a new location.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public Location(final double x, final double y) {
		fX = x;
		fY = y;
	}
	
	/**
	 * Calculate distance from a given distance.
	 *
	 * @param other the other distance
	 * @return the double
	 */
	public final double calculateDistance(final Location other) {
		return Math.sqrt(Math.pow((fX - other.fX), 2) + Math.pow((fY - other.fY), 2));
	}
}
