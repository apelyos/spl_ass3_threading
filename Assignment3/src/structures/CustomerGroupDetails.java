package structures;

import java.util.List;
import java.util.Queue;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import passive.Customer;

/**
 * The Class CustomerGroupDetails.
 */
@XmlRootElement(name = "customergroupdetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerGroupDetails {
	@XmlElement(name = "groupmanagername")
	private String fGroupManager;
	
	@XmlElementWrapper(name = "customers")
	@XmlElement(name = "customer")
	private List<Customer> fCustomers;
	
	@XmlElementWrapper(name = "rentalrequests")
	@XmlElement(name = "request")
	private Queue<RentalRequest> fRentalRequests;
	
	private CustomerGroupDetails() {
		// Empty constructor for JAXB
	}
	
	/**
	 * Poll rental request from the queue.
	 *
	 * @return the rental request
	 */
	public RentalRequest pollRentalRequest() {
		return fRentalRequests.poll();
	}
	
	/**
	 * Gets the customers.
	 *
	 * @return the customers
	 */
	public List<Customer> getCustomers() {
		return fCustomers;
	}
}
