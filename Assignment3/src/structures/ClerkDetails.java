package structures;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class ClerkDetails.
 */
@XmlRootElement(name = "clerk")
@XmlAccessorType(XmlAccessType.FIELD)
public class ClerkDetails {
	@XmlElement(name = "name")
	private String fName;
	
	@XmlElement(name = "location")
	private Location fLocation;
	
	private ClerkDetails() {
		// Empty constructor for JAXB
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return fName;
	}
	
	/**
	 * Gets the location.
	 *
	 * @return the location
	 */
	public Location getLocation() {
		return fLocation;
	}
}
