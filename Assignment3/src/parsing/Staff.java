package parsing;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import structures.ClerkDetails;


//Dummy object for parsing

@XmlRootElement(name = "staff")
@XmlAccessorType(XmlAccessType.FIELD)
public class Staff {
	
	@XmlElementWrapper(name = "clerks")
	@XmlElement(name = "clerk")
	private List<ClerkDetails> _clerks;
	
	@XmlElement(name = "numberofmaintenancepersons")
	private int _numberOfMaintenancePersons;
	
	@XmlElement(name = "totalnumberofrentalrequests")
	private int _totalNumberOfRentalRequests;
	
	public List<ClerkDetails> getClerks() {
		return _clerks;
	}
	
	public int getNumberOfMaintenancePersons() {
		return _numberOfMaintenancePersons;
	}
	
	public int getTotalNumberOfRentalRequests() {
		return _totalNumberOfRentalRequests;
	}
}