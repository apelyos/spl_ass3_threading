package parsing;

import java.util.concurrent.Semaphore;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class IntegerSemaphoreAdapter extends XmlAdapter<Integer, Semaphore> {
    @Override
    public Semaphore unmarshal(Integer number) throws Exception {
    	return new Semaphore(number);
    }

	@Override
	public Integer marshal(Semaphore semaphore) throws Exception {
		// We don't support marshaling
		return null;
	}
}
