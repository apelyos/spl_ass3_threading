package parsing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import structures.RepairMaterial;


// Hash Map adapter for RepairMaterial 
public class RepairMaterialsMapAdapter extends XmlAdapter<RepairMaterialsMapAdapter.Materials, Map<String, RepairMaterial>> {
	@XmlRootElement(name = "materials")
    static class Materials {
		@XmlElement(name = "material")
        public List<RepairMaterial> materials = new ArrayList<RepairMaterial>();
    }
     
    @Override
    public Map<String, RepairMaterial> unmarshal(RepairMaterialsMapAdapter.Materials list) throws Exception {
    	Map<String, RepairMaterial> map = new HashMap<String, RepairMaterial>();
        for (RepairMaterial material : list.materials) {
            map.put(material.getName(), material);
        }
        return map;
    }
 

	@Override
	public RepairMaterialsMapAdapter.Materials marshal(Map<String, RepairMaterial> map) throws Exception {
		// We don't support marshaling
		return null;
	}
 
}