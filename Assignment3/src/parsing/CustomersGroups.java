package parsing;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import structures.CustomerGroupDetails;

// Dummy object for parsing

@XmlRootElement(name = "customersgroups")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomersGroups {
	@XmlElementWrapper(name = "customergroups")
	@XmlElement(name = "customergroupdetails")
	private List<CustomerGroupDetails> _customerGroups;
	
	public List<CustomerGroupDetails> getCustomerGroups() {
		return _customerGroups;
	}
}
