package parsing;

import javax.xml.bind.annotation.XmlAccessorType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import structures.Warehouse;

//Dummy object for parsing

@XmlRootElement(name = "reit")
@XmlAccessorType(XmlAccessType.FIELD)
public class REIT {
	@XmlElement(name = "warehouse")
	private Warehouse _warehouse;
	
	@XmlElement(name = "staff")
	Staff _staff;
	
	public Warehouse getWarehouse() {
		return _warehouse;
	}
	
	public Staff getStaff() {
		return _staff;
	}
}
