package parsing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import structures.RepairTool;


// Hash Map adapter for RepairTool 
public class RepairToolsMapAdapter extends XmlAdapter<RepairToolsMapAdapter.Tools, Map<String, RepairTool>> {
	@XmlRootElement(name = "tools")
    static class Tools {
		@XmlElement(name = "tool")
        public List<RepairTool> tools = new ArrayList<RepairTool>();
    }
     
    @Override
    public Map<String, RepairTool> unmarshal(RepairToolsMapAdapter.Tools list) throws Exception {
    	Map<String, RepairTool> map = new HashMap<String, RepairTool>();
        for (RepairTool tool : list.tools) {
            map.put(tool.getName(), tool);
        }
        return map;
    }
 

	@Override
	public RepairToolsMapAdapter.Tools marshal(Map<String, RepairTool> map) throws Exception {
		// We don't support marshaling
		return null;
	}
 
}