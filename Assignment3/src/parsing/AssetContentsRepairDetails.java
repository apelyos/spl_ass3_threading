package parsing;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//Dummy object for parsing

@XmlRootElement(name = "assetcontentsrepairdetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class AssetContentsRepairDetails {
	
	@XmlElement(name = "assetcontent")
	private List<AssetContentsRepairDetail> _assetContentsRepairDetail;
	
	public List<AssetContentsRepairDetail> getAssetContentsRepairDetails() {
		return _assetContentsRepairDetail;
	}
}
