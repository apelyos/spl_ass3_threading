package parsing;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import structures.RepairMaterialInformation;
import structures.RepairToolInformation;

//Dummy object for parsing

@XmlRootElement(name = "assetcontent")
@XmlAccessorType(XmlAccessType.FIELD)
public class AssetContentsRepairDetail {
	@XmlElement(name = "name")
	private String _name;
	
	@XmlElementWrapper(name = "tools")
	@XmlElement(name = "tool")
	private List<RepairToolInformation> _repairToolInformation;
	
	@XmlElementWrapper(name = "materials")
	@XmlElement(name = "material")
	private List<RepairMaterialInformation> _repairMaterialInformation;
	
	public String getItemName() {
		return _name;
	}
	
	public List<RepairToolInformation> getToolsInfo() {
		return _repairToolInformation;
	}
	
	public List<RepairMaterialInformation> getMaterialsInfo() {
		return _repairMaterialInformation;
	}
}
