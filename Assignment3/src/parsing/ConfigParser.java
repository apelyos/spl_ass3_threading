package parsing;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;


// Generic Parser for the following XML files: InitialData CustomersGroups Assets AssetContentsRepairDetails
// And return the appropriate objects
public class ConfigParser {
	@SuppressWarnings("unchecked") // It's ok
	public static <T> T parse(Class<T> classToParse, String pathToXML) throws JAXBException, FileNotFoundException, XMLStreamException {
        XMLInputFactory xif = XMLInputFactory.newInstance();
        XMLStreamReader xsr = xif.createXMLStreamReader(new FileInputStream(pathToXML));
        xsr = new MyStreamReaderDelegate(xsr);
		JAXBContext jaxbContext = JAXBContext.newInstance(classToParse);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return (T) jaxbUnmarshaller.unmarshal(xsr);
	}
	

	
	// To ignore XML case
    private static class MyStreamReaderDelegate extends StreamReaderDelegate {
        public MyStreamReaderDelegate(XMLStreamReader xsr) {
            super(xsr);
        }

        @Override
        public String getAttributeLocalName(int index) {
            return super.getAttributeLocalName(index).toLowerCase();
        }

        @Override
        public String getLocalName() {
            return super.getLocalName().toLowerCase();
        }
    }
    


    
}
